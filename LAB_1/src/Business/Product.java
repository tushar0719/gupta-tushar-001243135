/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Product {
    
    private String name;
    private String Price;
    private String Availnumber;
    private String Des;

    public String getName() {
        return name;
    }

    public String getPrice() {
        return Price;
    }

    public String getAvailnumber() {
        return Availnumber;
    }

    public String getDes() {
        return Des;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(String Price) {
        this.Price = Price;
    }

    public void setAvailnumber(String Availnumber) {
        this.Availnumber = Availnumber;
    }

    public void setDes(String Des) {
        this.Des = Des;
    }

    
    
}
