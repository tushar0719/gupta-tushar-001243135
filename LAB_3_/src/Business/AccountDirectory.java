/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Tushar
 */
public class AccountDirectory {
    
    private ArrayList<Account> accountList;

    public AccountDirectory() {        
        this.accountList = new ArrayList<Account>();        
    }    
    
    public ArrayList<Account> getAccount() {
        return accountList;
    }

    public void setAccount(ArrayList<Account> account) {
        this.accountList = accountList;
    }
    
    public Account addAccount(){
        Account account = new Account();
        accountList.add(account);
        return account;
    }
    
    public void  deleteAccount(Account account){        
        accountList.remove(account);        
    }
    
    public Account searchAccount(String accountNumber){
        for(Account a: accountList){
            if(a.getAccountNumber().equalsIgnoreCase(accountNumber)){
                return a;
            }
        }
        return null;
        
    }
  
    
}
