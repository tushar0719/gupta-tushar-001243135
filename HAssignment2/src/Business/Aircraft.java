/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.FileReader;
import java.util.*;

/**
 *
 * @author Tushar
 */
public class Aircraft implements Comparable<Aircraft> {
    
    private String flightnumber;
    private String flighttime;
    private String company;
    private String mfgdate;
    private String seatsavailable;
    private String serialnumber;
    private String modelnumber;
    private String airport;
    private String mcdate;
    private String airliner;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getFlightnumber() {
        return flightnumber;
    }

    public void setFlightnumber(String flightnumber) {
        this.flightnumber = flightnumber;
    }

    public String getFlighttime() {
        return flighttime;
    }

    public void setFlighttime(String flighttime) {
        this.flighttime = flighttime;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getMfgdate() {
        return mfgdate;
    }

    public void setMfgdate(String mfgdate) {
        this.mfgdate = mfgdate;
    }

    public String getSeatsavailable() {
        return seatsavailable;
    }

    public void setSeatsavailable(String seatsavailable) {
        this.seatsavailable = seatsavailable;
    }

    public String getSerialnumber() {
        return serialnumber;
    }

    public void setSerialnumber(String serialnumber) {
        this.serialnumber = serialnumber;
    }

    public String getModelnumber() {
        return modelnumber;
    }

    public void setModelnumber(String modelnumber) {
        this.modelnumber = modelnumber;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getMcdate() {
        return mcdate;
    }

    public void setMcdate(String mcdate) {
        this.mcdate = mcdate;
    }

    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airliner) {
        this.airliner = airliner;
    }
    
    @Override
    public String toString()
    {
        return this.flightnumber;
    } 

    @Override
    public int compareTo(Aircraft o) {
        return this.getFlighttime().compareTo(o.getFlighttime());
    }
    
    
}
