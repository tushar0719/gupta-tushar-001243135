/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Seat {
    
    private String seatpos;
    private String seatno;

    public String getSeatno() {
        return seatno;
    }

    public void setSeatno(String seatno) {
        this.seatno = seatno;
    }

    public String getSeatpos() {
        return seatpos;
    }

    public void setSeatpos(String seatpos) {
        this.seatpos = seatpos;
    }
    
}
