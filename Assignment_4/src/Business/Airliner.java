/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Airliner {
    
    private String airliner;
    private String flightnumber;
    private String journeydate;    
    private String flighttime;      
    private String airportSource;
    private String airportDestination;    
    //private Seat seat;
    private int price;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
//    public Seat getSeat() {
//        return seat;
//    }
//
//    public void setSeat(Seat seat) {
//        this.seat = seat;
//    }
       
    
    public String getJourneydate() {
        return journeydate;
    }

    public void setJourneydate(String journeydate) {
        this.journeydate = journeydate;
    }
    public String getFlightnumber() {
        return flightnumber;
    }

    public void setFlightnumber(String flightnumber) {
        this.flightnumber = flightnumber;
    }

    public String getFlighttime() {
        return flighttime;
    }

    public void setFlighttime(String flighttime) {
        this.flighttime = flighttime;
    }

        public String getAirportSource() {
        return airportSource;
    }

    public void setAirportSource(String airportSource) {
        this.airportSource = airportSource;
    }

    public String getAirportDestination() {
        return airportDestination;
    }

    public void setAirportDestination(String airportDestination) {
        this.airportDestination = airportDestination;
    }

    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airliner) {
        this.airliner = airliner;
    }

    @Override
    public String toString() {
        return airliner;
    }
    
    
    
    
    
}
