/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author Tushar
 */
public class Flight  {
    
    private Seats seats;
    private Airplane airplane;
    private String source;
    private String destination;
    private String flighttime;
    private static int bookingNumberCount = 0;

    public Flight() {
        
       seats = new Seats();
       
//       for(int i = 0; i<=25 ; i++)
//       {
//           Seat[] seat = new Seat[6];
//           
//           for(int j=0; j<6 ; j++)
//           {
//               seat[j] = new Seat();
//               seat[j].setSeatavailable(true);
//           }
//           
//           seat[0].setSeatpos("leftwindow");
//           seat[1].setSeatpos("leftmiddle");
//           seat[2].setSeatpos("leftasile");
//           seat[3].setSeatpos("rightwindow");
//           seat[4].setSeatpos("rightmiddle");
//           seat[5].setSeatpos("rightasile");
//                   
//           
//       }

    }

    
    public static int getBookingNumberCount() {
        return bookingNumberCount;
    }

    public static void setBookingNumberCount(int bookingNumberCount) {
        Flight.bookingNumberCount = bookingNumberCount;
    }
    
    public Seats getSeats() {
        return seats;
    }

    public void setSeats(Seats seats) {
        this.seats = seats;
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFlighttime() {
        return flighttime;
    }

    public void setFlighttime(String flighttime) {
        this.flighttime = flighttime;
    }

    @Override
    public String toString() {
        return source ;
    }

        
    
    
    
}
