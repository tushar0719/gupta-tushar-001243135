/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class Airliner {
    
    private Schedule schedule;
    private ArrayList<Airplane> airplane;
    private String airlinerName;

    public String getAirlinerName() {
        return airlinerName;
    }

    public void setAirlinerName(String airlinerName) {
        this.airlinerName = airlinerName;
    }
    
    
    public Airliner(){
    ArrayList<Airplane> airplane;
        this.airplane = new ArrayList<Airplane>() ;
        schedule = new Schedule();
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public ArrayList<Airplane> getAirplane() {
        return airplane;
    }

    public void setAirplane(ArrayList<Airplane> airplane) {
        this.airplane = airplane;
    }
    public Airplane addairplanedetails(){
       
       Airplane  s  = new Airplane();
       airplane.add(s);
       return s;
           
   }

    @Override
    public String toString() {
        return airlinerName ;
    }
    
}
