/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class TravelAgency {
    
    private CustomerDirectory customerDirectory;
    private MasterScheduler masterScheduler;
    private AirlinerDirectory airlinerDirectory;

    public TravelAgency(CustomerDirectory customerDirectory, MasterScheduler masterScheduler, AirlinerDirectory airlinerDirectory) {
        this.customerDirectory = customerDirectory;
        this.masterScheduler = masterScheduler;
        this.airlinerDirectory = airlinerDirectory;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public MasterScheduler getMasterScheduler() {
        return masterScheduler;
    }

    public void setMasterScheduler(MasterScheduler masterScheduler) {
        this.masterScheduler = masterScheduler;
    }

    public AirlinerDirectory getAirlinerDirectory() {
        return airlinerDirectory;
    }

    public void setAirlinerDirectory(AirlinerDirectory airlinerDirectory) {
        this.airlinerDirectory = airlinerDirectory;
    }
    
    
    
}
