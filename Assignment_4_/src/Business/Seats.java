/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class Seats {
    
    private ArrayList<Seat[]> seat = new ArrayList<Seat[]>() ;  
    

    public Seats() {
        seat = new ArrayList<Seat[]>();
        for(int i=0;i<25;i++)
        {
            addSeatRow();
            
        }
    }

    public ArrayList<Seat[]> getSeat() {
        return seat;
    }

    public void setSeat(ArrayList<Seat[]> seat) {
        this.seat = seat;
    }

    public void addSeatRow()
    {
        Seat[] seatRow = new Seat[6];
        Seat seat1 = new Seat("left-window");
        Seat seat2 = new Seat("left-middle");
        Seat seat3 = new Seat("left-aisle");
        Seat seat4 = new Seat("right-window");
        Seat seat5 = new Seat("right-middle");
        Seat seat6 = new Seat("right-aisle");
        seat1.setSeatavailable(Boolean.TRUE);
        seat2.setSeatavailable(Boolean.TRUE);
        seat3.setSeatavailable(Boolean.TRUE);
        seat4.setSeatavailable(Boolean.TRUE);
        seat5.setSeatavailable(Boolean.TRUE);
        seat6.setSeatavailable(Boolean.TRUE);
        
        seatRow[0] = seat1;
        seatRow[1] = seat2;
        seatRow[2] = seat3;
        seatRow[3] = seat4;
        seatRow[4] = seat5;
        seatRow[5] = seat6;
        
        this.seat.add(seatRow);
        
        
        
    }

    @Override
    public String toString() {
        return "Seats{" + "seat=" + seat + '}';
    }
    
}
