/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Seat {
    
    private String seatpos;
    private String seatprice;
    private Boolean seatavailable;
    private Booking booking;

    public String getSeatpos() {
        return seatpos;
    }

    public Boolean getSeatavailable() {
        return seatavailable;
    }

    public void setSeatavailable(Boolean seatavailable) {
        this.seatavailable = seatavailable;
    }

    public void setSeatpos(String seatpos) {
        this.seatpos = seatpos;
    }

    public boolean isSeatavailable() {
        return seatavailable;
    }

    public void setSeatavailable(boolean seatavailable) {
        this.seatavailable = seatavailable;
    }

    public Seat(String seatpos) {
        this.seatpos = seatpos;
    }

    

    public String getSeatprice() {
        return seatprice;
    }

    public void setSeatprice(String seatprice) {
        this.seatprice = seatprice;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }
    
    public Booking addBoooking()
    {
        booking = new Booking();
        return booking;
    }
    
}
