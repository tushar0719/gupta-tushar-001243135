/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Booking {
    
    private Customer customer;
    static int bookingNumberCounter = 0;    
    private int bookingNumber;
    
    public Booking()
    {
        
    }
    
    public Booking(Customer customer) {
        this.customer = customer;
        bookingNumber = ++bookingNumberCounter;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    public int getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(int bookingNumber) {
        this.bookingNumber = bookingNumber;
    }
    
}
