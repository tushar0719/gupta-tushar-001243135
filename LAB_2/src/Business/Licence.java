/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Licence {
    
    private String licencenumber;
    private String dateofissue ;
    private String residentcity;
    private String dateofexpiry;

    public String getLicencenumber() {
        return licencenumber;
    }

    public String getDateofissue() {
        return dateofissue;
    }

    public String getResidentcity() {
        return residentcity;
    }

    public String getDateofexpiry() {
        return dateofexpiry;
    }

    public void setLicencenumber(String licencenumber) {
        this.licencenumber = licencenumber;
    }

    public void setDateofissue(String dateofissue) {
        this.dateofissue = dateofissue;
    }

    public void setResidentcity(String residentcity) {
        this.residentcity = residentcity;
    }

    public void setDateofexpiry(String dateofexpiry) {
        this.dateofexpiry = dateofexpiry;
    }
    
    
    
    
}
