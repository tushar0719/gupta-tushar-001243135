/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Spouse {
    
    private String name1;
    private String DOB1;
    private String bloodgroup1;
    private String gender1;

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getDOB1() {
        return DOB1;
    }

    public void setDOB1(String DOB1) {
        this.DOB1 = DOB1;
    }

    public String getBloodgroup1() {
        return bloodgroup1;
    }

    public void setBloodgroup1(String bloodgroup1) {
        this.bloodgroup1 = bloodgroup1;
    }

    public String getGender1() {
        return gender1;
    }

    public void setGender1(String gender1) {
        this.gender1 = gender1;
    }
    
    
}
