/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;


/**
 *
 * @author Tushar
 */
public class Person {
    
    private String name;
    private String DOB;
    private String bloodgroup;
    private String gender;
    private Spouse spouse;
    private Address workaddress;
    private Licence licen;
    private Creditcard credit;
    private Finance fin;
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Spouse getSpouse() {
        return spouse;
    }

    public void setSpouse(Spouse spouse) {
        this.spouse = spouse;
    }

    
    
      

    public Licence getLicen() {
        return licen;
    }

    public void setLicen(Licence licen) {
        this.licen = licen;
    }

    public Creditcard getCredit() {
        return credit;
    }

    public void setCredit(Creditcard credit) {
        this.credit = credit;
    }

    public Finance getFin() {
        return fin;
    }

    public void setFin(Finance fin) {
        this.fin = fin;
    }
    
    
    
    
    public Address getWorkAddress(){
            return workaddress;           
    }
    
    public void setWorkAddress(Address addresspa){
        this.workaddress = addresspa;

    }
    
    public String getName() {
        return name;
    }

    public String getDOB() {
        return DOB;
    }

    public String getbloodgroup() {
        return bloodgroup;
    }

    public String getGender() {
        return gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public void setBloodgroup(String Bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    
    }

    
    

