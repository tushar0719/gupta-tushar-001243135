/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Creditcard {
    
    private String cardnumber;
    private String dateofissue;
    private String dateofexpiry;
    private String mastervisa;
    private String cvv;

    public String getCardnumber() {
        return cardnumber;
    }

    public String getDateofissue() {
        return dateofissue;
    }

    public String getDateofexpiry() {
        return dateofexpiry;
    }

    public String getMastervisa() {
        return mastervisa;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public void setDateofissue(String dateofissue) {
        this.dateofissue = dateofissue;
    }

    public void setDateofexpiry(String dateofexpiry) {
        this.dateofexpiry = dateofexpiry;
    }

    public void setMastervisa(String mastervisa) {
        this.mastervisa = mastervisa;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
    
    
}
