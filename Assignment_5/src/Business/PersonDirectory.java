/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class PersonDirectory {
    
    private ArrayList<Person> personList;
    
    public PersonDirectory(){
        
        personList = new ArrayList();
        
    }

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }
    
    
    public Person addPerson(){
        Person p = new Person();
        personList.add(p);
        return p;
    }
    
    public void removePerson(Person p){
        
        personList.remove(p);
    }
    
    public Person isPersonPresent(String fname, String Lname){
        
        for(Person p: personList){
            if(fname.equals(p.getFname()) && Lname.equals(p.getLname())){
                return p;
            }
        }
        return null;
    }
    
    
}
