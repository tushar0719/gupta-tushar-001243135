/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class UserAccountDirectory {
    
    
    private ArrayList<UserAccount> userList ;

    public UserAccountDirectory() {
        
        userList = new ArrayList<UserAccount>();
        
    }

    public ArrayList<UserAccount> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<UserAccount> userList) {
        this.userList = userList;
    }
    
    public UserAccount addUser(){
        
        UserAccount u = new UserAccount();
        userList.add(u);
        return u;                
    }
    public void removeUser(UserAccount u){
        
        userList.remove(u);
    }
    public UserAccount isValidUser(String userid, String Pass)
    {
        for(UserAccount u: userList){
            if(userid.equals(u.getUserId()) && Pass.equals(u.getPassword())){
                return u;
            }
        }
        return null;
    }
    
    
}
