/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class ConfigureABusiness {
    
    public static  Business Initialize (String n){
        
        Business b = new Business(n);
        
        PersonDirectory pd = b.getPersonDirectory();
        
        Person p1 = pd.addPerson();
        p1.setFname("Tushar");
        p1.setLname("Gupta");
        p1.setUserID("tgupta");
        p1.setSsn("1234");
        
        Person p2 = pd.addPerson();
        p2.setFname("Mitali");
        p2.setLname("Gupta");
        p2.setUserID("mgupta");
        p2.setSsn("5678");
        
        Person p = pd.addPerson();
        p.setFname("Suman");
        p.setLname("Gupta");
        p.setUserID("sgupta");
        p.setSsn("91015");
        
        
        UserAccountDirectory uad = b.getUserAccountDirectory();        
        UserAccount u = uad.addUser();
        UserAccount u1 = uad.addUser();
        UserAccount u2 = uad.addUser();
        
        u.setPerson(p1);
        u.setUserId("tgupta");
        u.setPassword("tgupta");
        u.setAccountType("System Admin");
        u.setStatus(true);
        
        u1.setPerson(p2);
        u1.setUserId("mgupta");
        u1.setPassword("mgupta");
        u1.setAccountType("HR Manager"); 
        u1.setStatus(true);
        
        return b;
    }
    
}
