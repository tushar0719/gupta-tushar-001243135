/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author joshiaditya
 */
public class EmployeeDirectory {
    
    private ArrayList<Employee> employeeDirectory;
    
    public EmployeeDirectory(){
        
        employeeDirectory = new ArrayList<Employee>();
    }

    
   
    
    public ArrayList<Employee> getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(ArrayList<Employee> employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }
    
    public Employee addEmployee(){
        Employee e = new Employee();
        employeeDirectory.add(e);
        return e;
    }
    
    
    public void removeEmployee(Employee e){
        
        employeeDirectory.remove(e);
        
    }
    
    public Employee isValidUser(String userid, String Pass,String role)
    {
        for(Employee e: employeeDirectory){
            if(userid.equals(e.getUsername()) && Pass.equals(e.getPassowrd()) && role.equals(e.getDesignation())){
                return e;
            }
        }
        return null;
    }

    
}
