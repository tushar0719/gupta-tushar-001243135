/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class ProductCatalogue {
    
    private ArrayList<Product> productList;
    
    public ProductCatalogue (){
        productList = new ArrayList<Product>();
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }    
    
    public Product addproduct(){
        Product p = new Product();
        productList.add(p);
        return p;
    }
    
     public void removeProduct(Product p){
        productList.remove(p);
    }
     
     public Product searchProduct(int id){
        for (Product product : productList) {
            if(product.getProductID()==id){
                return product;
            }
        }
        return null;
    }
     
     
}
