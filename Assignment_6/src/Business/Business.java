/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Business {
    
    private SupplierDirectory supplierDirectory;
    private CustomerDirectory customerDirectory;
    private MarketList marketList;
    private EmployeeDirectory employeeDirectory;
    private PersonDirectory personDirectory;
    private MasterOrderCatalog masterOrderCatalog;
    private MarketOfferCatalogue marketOfferCatalogue;
    
    public Business() {      
        
        supplierDirectory = new SupplierDirectory();
        customerDirectory = new CustomerDirectory();
        marketList = new MarketList();
        employeeDirectory = new EmployeeDirectory();
        personDirectory = new PersonDirectory();
        masterOrderCatalog = new MasterOrderCatalog();
        marketOfferCatalogue = new MarketOfferCatalogue();
    }

    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public MarketList getMarketList() {
        return marketList;
    }

    public void setMarketList(MarketList marketList) {
        this.marketList = marketList;
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }

    public MarketOfferCatalogue getMarketOfferCatalogue() {
        return marketOfferCatalogue;
    }

    public void setMarketOfferCatalogue(MarketOfferCatalogue marketOfferCatalogue) {
        this.marketOfferCatalogue = marketOfferCatalogue;
    }

}
