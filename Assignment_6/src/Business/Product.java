/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Product implements Comparable<Product> {
    
    private String productName;
    private int productID;    
    private int floorprice;
    private int actualprice;
    private int ceilprice;
    private int topcount;

    public int getTopcount() {
        return topcount;
    }

    public void setTopcount(int topcount) {
        this.topcount = topcount;
    }    
    
    private static int count = 0;
    
    public Product() {
        count++;
        productID = count;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getFloorprice() {
        return floorprice;
    }

    public void setFloorprice(int floorprice) {
        this.floorprice = floorprice;
    }

    public int getActualprice() {
        return actualprice;
    }

    public void setActualprice(int actualprice) {
        this.actualprice = actualprice;
    }

    public int getCeilprice() {
        return ceilprice;
    }

    public void setCeilprice(int ceilprice) {
        this.ceilprice = ceilprice;
    }  
    
    @Override
    public String toString() {
        return String.valueOf(productID);
    }

    @Override
    public int compareTo(Product o) {
         return Integer.compare( o.getTopcount(),this.getTopcount());
    }
    
}
