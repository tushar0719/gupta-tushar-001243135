/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author joshiaditya
 */
public class MarketOfferCatalogue {
    
    private ArrayList<MarketOffer> marketOfferCatalogue;

    public MarketOfferCatalogue() {
        marketOfferCatalogue = new ArrayList<MarketOffer>();
    }

    public ArrayList<MarketOffer> getMarketOfferCatalogue() {
        return marketOfferCatalogue;
    }

    public void setMarketOfferCatalogue(ArrayList<MarketOffer> marketOfferCatalogue) {
        this.marketOfferCatalogue = marketOfferCatalogue;
    }
    
    public MarketOffer addMarketOffer(){
        MarketOffer mo = new MarketOffer();
        marketOfferCatalogue.add(mo);
        return mo;
    }
}
