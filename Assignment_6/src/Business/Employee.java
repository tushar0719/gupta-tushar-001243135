/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author joshiaditya
 */
public class Employee implements Comparable<Employee>{
    
    private String designation;
    private int commission = 0;
    private String username;
    private String passowrd;
    private Person person;
    private ArrayList<Order> employeeOrderList;
    private boolean salesAboveTarget ;
    private boolean salesBelowTarget ;
    private int lossOrProfit;
    private int topCount;
    
    public int getLossOrProfit() {
        return lossOrProfit;
    }

    public void setLossOrProfit(int lossOrProfit) {
        this.lossOrProfit = lossOrProfit;
    }
    
    
    public boolean isSalesAboveTarget() {
        return salesAboveTarget;
    }

    public void setSalesAboveTarget(boolean salesAboveTarget) {
        this.salesAboveTarget = salesAboveTarget;
    }

    public boolean isSalesBelowTarget() {
        return salesBelowTarget;
    }

    public void setSalesBelowTarget(boolean salesBelowTarget) {
        this.salesBelowTarget = salesBelowTarget;
    }
    
    public Employee(){        
        this.person = new Person();
        this.employeeOrderList = new ArrayList<Order>();
    }
    
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassowrd() {
        return passowrd;
    }

    public void setPassowrd(String passowrd) {
        this.passowrd = passowrd;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }   

    public ArrayList<Order> getEmployeeOrderList() {
        return employeeOrderList;
    }

    public void setEmployeeOrderList(ArrayList<Order> employeeOrderList) {
        this.employeeOrderList = employeeOrderList;
    }
    
    @Override
    public String toString() {
        return username;
    }
    
    public Order addOrder(Order o)
    {
        employeeOrderList.add(o);
        return o;
    }

    public int getTopCount() {
        return topCount;
    }

    public void setTopCount(int topCount) {
        this.topCount = topCount;
    }
    
    @Override
    public int compareTo(Employee o) {
         return Integer.compare(o.getTopCount(),this.getTopCount());
    }
}
