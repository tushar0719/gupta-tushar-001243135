/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Tushar
 */
public class ConfigurationBusiness {
    
    public static  Business Initialize (){
        
        Business b = new Business();
        
        String csvFileCustomer = "CustomerList.csv";        
        String csvFileEmployee = "EmployeeList.csv";
        String csvFileMarket = "MarketList.csv";        
        String csvFilePeroson = "PersonList.csv";
        String csvFileProduct = "ProductList.csv";        
        String csvFileSupplier = "SupplierList.csv";
        
        BufferedReader br = null;        
        BufferedReader br1 = null;
        BufferedReader br2 = null;
        BufferedReader br3 = null;
        BufferedReader br4 = null;
        BufferedReader br5 = null;
        
        String line = "";
        String cvsSplitBy = ",";
        
        MarketList marketList = new MarketList();
        try {
            br = new BufferedReader(new FileReader(csvFileMarket));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] marketElement = line.split(cvsSplitBy);
                Market market = marketList.addMarket();
                String name = marketElement[0];    
                int threshold = Integer.parseInt(marketElement[1]);
                market.setName(name);  
                market.setThreshold(threshold);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            if (br != null) {   
                try {
                    br.close();
                } 
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        CustomerDirectory customerDirectory = new CustomerDirectory();        
        try {
            br1 = new BufferedReader(new FileReader(csvFileCustomer));
            while ((line = br1.readLine()) != null) {
                // use comma as separator
                String[] customerElement = line.split(cvsSplitBy);
                Customer customer = customerDirectory.addCustomer();
                
                String name = customerElement[0];     
                String market = customerElement[1];
                customer.setName(name);
                
                for (Market m : marketList.getMarketList()){
                    if(m.getName().trim().length() == market.length()){
                        customer.setMarket(m);
                        break;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            if (br != null) {   
                try {
                    br.close();
                } 
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        PersonDirectory personDirectory = new PersonDirectory();
        try {
            br2 = new BufferedReader(new FileReader(csvFilePeroson));
            while ((line = br2.readLine()) != null) {
                // use comma as separator
                String[] personElement = line.split(cvsSplitBy);
                Person person = personDirectory.addPerson();
                String name = personElement[0]; 
                int ssn = Integer.parseInt(personElement[1]);
                int contact = Integer.parseInt(personElement[2]);
                person.setName(name);
                person.setSocialSecurity(ssn);
                person.setContact(contact);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (br != null) {   
                try {
                    br.close();
                } 
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        EmployeeDirectory employeeDirectory = new EmployeeDirectory();        
        try {
            br3 = new BufferedReader(new FileReader(csvFileEmployee));
            while ((line = br3.readLine()) != null) {
                // use comma as separator
                String[] employeeElement = line.split(cvsSplitBy);
                Employee employee = employeeDirectory.addEmployee();
                String name = employeeElement[0];
                String username = employeeElement[1];
                String password = employeeElement[2];   
                String designation = employeeElement[3];
                int topcount = Integer.parseInt(employeeElement[4]);
                
                for( Person p : personDirectory.getPersonDirectory()){
                    if(p.getName().compareTo(name) == 0){
                        employee.setPerson(p);
                        break;
                    }
                }
                employee.setUsername(username);
                employee.setPassowrd(password);
                employee.setDesignation(designation);
                employee.setTopCount(topcount);
                employee.setSalesAboveTarget(true);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (br != null) {   
                try {
                    br.close();
                } 
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        SupplierDirectory supplierDirectory = new SupplierDirectory();
        try {
            br4 = new BufferedReader(new FileReader(csvFileSupplier));
            while ((line = br4.readLine()) != null) {

                // use comma as separator
                String[] supplierElement = line.split(cvsSplitBy);
                Supplier supplier = supplierDirectory.addSupplier();
                String name = supplierElement[0];                               
                supplier.setSupplierName(name);  
                
                ProductCatalogue productCatalogue = new ProductCatalogue();
                br5 = new BufferedReader(new FileReader(csvFileProduct));
                while ((line = br5.readLine()) != null) {

                    // use comma as separator
                    String[] productElement = line.split(cvsSplitBy);
                    if( name.trim().equals(productElement[5].trim()) ){
                        
                        Product product = productCatalogue.addproduct();
                        String productName = productElement[0];
                        int floorPrice = Integer.parseInt(productElement[4]);
                        int actualPrice = Integer.parseInt(productElement[3]);
                        int ceilPrice = Integer.parseInt(productElement[2]);                        

                        product.setProductName(productName);
                        product.setActualprice(actualPrice);
                        product.setCeilprice(ceilPrice);
                        product.setFloorprice(floorPrice);
                        
                    }
                    
                }
                
                supplier.setProductCatalogue(productCatalogue);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (br != null) {   
                try {
                    br.close();
                } 
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        MarketOfferCatalogue marketOfferCatalogue = new MarketOfferCatalogue();
        try{
            for(Market mkt : marketList.getMarketList()){
            
            int threshold = mkt.getThreshold();
            MarketOffer marketOffer = marketOfferCatalogue.addMarketOffer();
            marketOffer.setMarketName(mkt.getName());
            SupplierDirectory marketSupDir = new SupplierDirectory();
            
                for(Supplier sup : supplierDirectory.getSupplierList()){
                    Supplier marketOfferSupplier = marketSupDir.addSupplier();
                    marketOfferSupplier.setSupplierName(sup.getSupplierName());
                    ProductCatalogue marketProdCat = new ProductCatalogue();

                    for(Product prod : sup.getProductCatalogue().getProductList()){
                        Product marketProduct = marketProdCat.addproduct();
                        marketProduct.setProductID(prod.getProductID());
                        marketProduct.setProductName(prod.getProductName());

                        int floorPrice = (prod.getFloorprice() * threshold)/100;
                        int ceilPrice = (prod.getCeilprice() * threshold)/100;
                        int actualPrice = (prod.getActualprice() * threshold)/100;
                        marketProduct.setFloorprice(floorPrice);
                        marketProduct.setCeilprice(ceilPrice);
                        marketProduct.setActualprice(actualPrice);
                    }
                    marketOfferSupplier.setProductCatalogue(marketProdCat); 
                }
                marketOffer.setMarketOffer(marketSupDir);
            }
        } catch (Exception e){
            
        }
        
        b.setCustomerDirectory(customerDirectory);
        b.setEmployeeDirectory(employeeDirectory);
        b.setMarketList(marketList);
        b.setPersonDirectory(personDirectory);
        b.setSupplierDirectory(supplierDirectory);
        b.setMarketOfferCatalogue(marketOfferCatalogue);
        return b;
    }
}
