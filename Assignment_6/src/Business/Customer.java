/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author joshiaditya
 */
public class Customer {
    
    private String name;
    private Market market;
    private ArrayList<Order> customerOrderList;
    
    public Customer(){
        
        this.market = new Market();
        this.customerOrderList = new ArrayList<Order>();
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public ArrayList<Order> getCustomerOrderList() {
        return customerOrderList;
    }

    public void setCustomerOrderList(ArrayList<Order> customerOrderList) {
        this.customerOrderList = customerOrderList;
    }
    
    public Order addOrder(Order o)
    {
        customerOrderList.add(o);
        return o;
    }
    
}
