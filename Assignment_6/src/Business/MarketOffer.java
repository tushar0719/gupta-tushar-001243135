/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author joshiaditya
 */
public class MarketOffer {
    
    private String marketName;
    private SupplierDirectory marketOffer;
    
    public MarketOffer(){
        marketOffer = new SupplierDirectory();
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public SupplierDirectory getMarketOffer() {
        return marketOffer;
    }

    public void setMarketOffer(SupplierDirectory marketOffer) {
        this.marketOffer = marketOffer;
    }
}
