/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class MasterSchedulerdirectory {
    
    private ArrayList<MasterScheduler> master = new ArrayList<MasterScheduler>();

    public ArrayList<MasterScheduler> getMaster() {
        return master;
    }

    public void setMaster(ArrayList<MasterScheduler> master) {
        this.master = master;
    }
    
    public MasterScheduler addmaster(){
       
       MasterScheduler  s  = new MasterScheduler();
       master.add(s);
       
       return s;
       
           
   }

    @Override
    public String toString() {
        return "MasterSchedulerdirectory{" + "master=" + master + '}';
    }
    
   
}
