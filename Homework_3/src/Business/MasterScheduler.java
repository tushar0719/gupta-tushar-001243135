/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class MasterScheduler {    
    
    private String flightnumber;
    private String flighttime;
    private String company;
    private String airport;
    private String airliner;
    private String Name;
    private String Dob;
    private String Age;    
    private String custseat;
    private String custseatpref;
    private int price;

    public String getFlightnumber() {
        return flightnumber;
    }

    public void setFlightnumber(String flightnumber) {
        this.flightnumber = flightnumber;
    }

    public String getFlighttime() {
        return flighttime;
    }

    public void setFlighttime(String flighttime) {
        this.flighttime = flighttime;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airliner) {
        this.airliner = airliner;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String Dob) {
        this.Dob = Dob;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String Age) {
        this.Age = Age;
    }

    

    public String getCustseat() {
        return custseat;
    }

    public void setCustseat(String custseat) {
        this.custseat = custseat;
    }

    public String getCustseatpref() {
        return custseatpref;
    }

    public void setCustseatpref(String custseatpref) {
        this.custseatpref = custseatpref;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }   

    @Override
    public String toString() {
        return "MasterScheduler{" + "flightnumber=" + flightnumber + ", flighttime=" + flighttime + ", company=" + company + ", airport=" + airport + ", airliner=" + airliner + ", Name=" + Name + ", Dob=" + Dob + ", Age=" + Age + ", custseat=" + custseat + ", custseatpref=" + custseatpref + ", price=" + price + '}';
    }
    
}
