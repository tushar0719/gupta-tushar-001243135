/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;


import Business.*;
import Business.Airlines;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class Travelagent {
    public static void main(String[] args) {
        // TODO code application logic here
        
        int priceqatar = 0;
        int priceluf = 0;
        int priceba = 0, price1D58 = 0,price1D67 = 0, price1D70 = 0,pricetotal = 0 ;
        
        String csvFile = "Airlines2.csv";        
        String csvFile2 = "customer_data.csv";
        
        BufferedReader br = null;        
        BufferedReader br2 = null;
        
        String line = "";
        String cvsSplitBy = ",";
        
        Airlines airplaneList = new Airlines();
        Aircraft aircraftlist = new Aircraft();
        Customerdirectory custd = new Customerdirectory();
        MasterSchedulerdirectory master = new MasterSchedulerdirectory();
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] airplaneElement = line.split(cvsSplitBy);
                Aircraft airplane = airplaneList.adddetails();
                String flightnumber = airplaneElement[0];
                String flighttime = airplaneElement[1];
                String company = airplaneElement[2];
                String airport = airplaneElement[3];
                String airliner = airplaneElement[4];
                String status   = airplaneElement[5];
                String seat = airplaneElement[6];
                String seatpos = airplaneElement[7];
                int price = Integer.parseInt(airplaneElement[8]);
                
               
                
                airplane.setFlightnumber(flightnumber);
                airplane.setFlighttime(flighttime);
                airplane.setCompany(company);
                airplane.setAirport(airport);
                airplane.setAirliner(airliner);
                airplane.setStatus(status);
                airplane.setSeatno(seat);
                airplane.setSeatpos(seatpos);
                airplane.setPrice(price);
            }
//            int i=1;
//            for (Aircraft a : airplaneList.getAircraft()) {
//                System.out.println(i+":Flight Number-->"+a.getFlightnumber()+
//                        "\tFlighttime-->"+a.getFlighttime()+
//                        "\tCompany-->"+a.getCompany()+
//                        "\tAirport-->"+a.getAirport()+
//                        "\tAirliner-->"+a.getAirliner()+
//                        "\tStatus-->"+a.getStatus()+
//                        "\tSeat number -->"+a.getSeatno()+
//                        "\tseat pos-->"+a.getSeatpos()+
//                        "\tPrice-->"+a.getPrice());
//                i++;
//                
//
//            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        
        
        
        
        try {

            br2 = new BufferedReader(new FileReader(csvFile2));
            while ((line = br2.readLine()) != null) {

                // use comma as separator
                String[] customerelement = line.split(cvsSplitBy);
                Customer  cust = custd.addcustdetails();
                String name = customerelement[0];
                String Dob = customerelement[1];
                String age = customerelement[2];
                String custflightno = customerelement[3];
                String custseatno = customerelement[4];
                String custseatpref = customerelement[5];
                
                cust.setName(name);
                cust.setDob(Dob);
                cust.setAge(age);
                cust.setCustflightno(custflightno);
                cust.setCustseat(custseatno);
                cust.setCustseatpref(custseatpref);
                
            }
//            int i=1;
//            for (Customer c : custd.getCust()) {
//                System.out.println(i+":Flight Number-->"
//                        +c.getName()+"\tFlighttime-->"
//                        +c.getDob()+"\tCompany-->"
//                        +c.getAge()+"\tAirport-->"
//                        +c.getCustflightno()+"\tAirliner-->"
//                        +c.getCustseat()+"\tStatus-->"+c.getCustseatpref());
//                i++;
//                
//
//            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br2 != null) {
                try {
                    br2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
           for(Customer c : custd.getCust())    
        {
             for(Aircraft vs : airplaneList.getAircraft())       
            {
                if(vs.getFlightnumber().equals(c.getCustflightno()))
                {
                    
                    if(vs.getSeatno().equals(c.getCustseat()))
                    {
                       if(vs.getSeatpos().equals(c.getCustseatpref()))
                       {
                          pricetotal = pricetotal + vs.getPrice();
                          
                         if(vs.getAirliner().equals("Qatar"))
                         {
                             priceqatar = priceqatar + vs.getPrice();
                         }
                         
                         if(vs.getAirliner().equals("Lufthansa"))
                         {
                             priceluf = priceluf + vs.getPrice();
                         }
                         
                         if(vs.getAirliner().equals("BA"))
                         {
                             priceba = priceba + vs.getPrice();                            
                         }
                         
                         if(vs.getFlightnumber().equals("1D58"))
                         {
                             price1D58 = price1D58 + vs.getPrice();
                         }
                         
                         if(vs.getFlightnumber().equals("1D67"))
                         {
                             price1D67 = price1D67 + vs.getPrice();
                         }
                         
                         if(vs.getFlightnumber().equals("1D70"))
                         {
                             price1D70 = price1D70 + vs.getPrice();
                         }
                         
                       }
                    }                                                          
                }
            }
        }  
        
        System.out.println("Revenue Generation");   
        System.out.println("Price of all Qatar    : "+ priceqatar);
        System.out.println("Price of all Luftansa : "+ priceluf);
        System.out.println("Price of all BA       : "+ priceba);
        System.out.println("Price of flight 1D158 : "+ price1D58);
        System.out.println("Price of flight 1D167 : "+ price1D67);
        System.out.println("Price of flight 1D170 : "+ price1D70);
        System.out.println("Price of All Flights :  "+ pricetotal);
        
        
        
//        for(MasterScheduler m : master.getMaster())
//        {
           for(Customer c : custd.getCust()) 
           {
               for(Aircraft vs : airplaneList.getAircraft())
               {
                  if(vs.getFlightnumber().equals(c.getCustflightno()) && vs.getSeatno().equals(c.getCustseat())
                  && vs.getSeatpos().equals(c.getCustseatpref()))
                {
                
                MasterScheduler mas = master.addmaster();
                String flightnumber = vs.getFlightnumber();
                String flighttime = vs.getFlighttime();
                String company = vs.getCompany();
                String airport = vs.getAirport();
                String airliner = vs.getAirliner();
                String Name = c.getName();
                String Dob = c.getDob();
                String Age = c.getAge();                
                String custseat = c.getCustseat();
                String custseatpref = c.getCustseatpref();
                int price = vs.getPrice();
                
                             
                mas.setFlightnumber(flightnumber);
                mas.setFlighttime(flighttime);
                mas.setCompany(company);
                mas.setAirport(airport);
                mas.setAirliner(airliner);
                mas.setName(Name);
                mas.setDob(Dob);
                mas.setAge(Age);
                mas.setPrice(price);
                mas.setCustseat(custseat);
                mas.setCustseatpref(custseatpref);
                          
               }
           }
        }
//    }
    int j = 1;
        System.out.println(" ");
    System.out.println("Master Scheduler");
    for(MasterScheduler m : master.getMaster())
    {
        System.out.println( j+":Flight Number-->"+m.getFlightnumber()
                        +"\tFlighttime-->"+m.getFlighttime()
                        +"\tCompany-->"+m.getCompany()
                        +"\tAirport-->"+m.getAirport()
                        +"\tAirliner-->"+m.getAirliner()
                        +"\tCustomer Name-->"+m.getName()
                        +"\tCustomer DOB-->"+m.getDob()
                        +"\tAge-->"+m.getAge()
                        +"\tFlight Price-->"+m.getPrice()
                        +"\tSeat-->"+m.getCustseat()
                        +"\tSeat pref-->"+m.getCustseatpref() );
        
        //System.out.println(m);
        
        j++;
    }
    
    
  }
}


