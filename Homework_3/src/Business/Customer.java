/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Tushar
 */
public class Customer {
    
    private String Name;
    private String Dob;
    private String Age;
    private String custflightno;
    private String custseat;
    private String custseatpref;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String Dob) {
        this.Dob = Dob;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String Age) {
        this.Age = Age;
    }

    public String getCustflightno() {
        return custflightno;
    }

    public void setCustflightno(String custflightno) {
        this.custflightno = custflightno;
    }

    public String getCustseat() {
        return custseat;
    }

    public void setCustseat(String custseat) {
        this.custseat = custseat;
    }

    public String getCustseatpref() {
        return custseatpref;
    }

    public void setCustseatpref(String custseatpref) {
        this.custseatpref = custseatpref;
    }

    @Override
    public String toString() {
        return "Customer{" + "Name=" + Name + ", Dob=" + Dob + ", Age=" + Age + ", custflightno=" + custflightno + ", custseat=" + custseat + ", custseatpref=" + custseatpref + '}';
    }
    
    
}
