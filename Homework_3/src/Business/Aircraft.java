/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.FileReader;
import java.util.*;

/**
 *
 * @author Tushar
 */
public class Aircraft implements Comparable<Aircraft> {
    
    private String flightnumber;
    private String flighttime;
    private String company;
    private String airport;
    private String airliner;
    private String status;
    private String seatno;
    private String seatpos;
    private int price;

    public String getSeatno() {
        return seatno;
    }

    public void setSeatno(String seatno) {
        this.seatno = seatno;
    }

    public String getSeatpos() {
        return seatpos;
    }

    public void setSeatpos(String seatpos) {
        this.seatpos = seatpos;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    

    
    
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getFlightnumber() {
        return flightnumber;
    }

    public void setFlightnumber(String flightnumber) {
        this.flightnumber = flightnumber;
    }

    public String getFlighttime() {
        return flighttime;
    }

    public void setFlighttime(String flighttime) {
        this.flighttime = flighttime;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }    

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getAirliner() {
        return airliner;
    }

    public void setAirliner(String airliner) {
        this.airliner = airliner;
    }
    
   
    @Override
    public int compareTo(Aircraft o) {
        return this.getFlighttime().compareTo(o.getFlighttime());
    }

    @Override
    public String toString() {
        return "Aircraft{" + "flightnumber=" + flightnumber + ", flighttime=" + flighttime + ", company=" + company + ", airport=" + airport + ", airliner=" + airliner + ", status=" + status + ", seatno=" + seatno + ", seatpos=" + seatpos + ", price=" + price + '}';
    }
    
    
}
