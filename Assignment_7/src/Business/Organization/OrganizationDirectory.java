/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.CDCOrganization.getValue())){
            organization = new CDCOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.SupplierOrganization.getValue())){
            organization = new SupplierOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.StateOrganization.getValue())){
            organization = new StateOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.DistributorOrganization.getValue())){
            organization = new DistributorOrganization();
            organizationList.add(organization);
        }
        
        return organization;
    }
}