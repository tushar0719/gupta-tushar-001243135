/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.ClerkRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class StateOrganization extends Organization{

    public StateOrganization() {
        super(Organization.Type.StateOrganization.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new ClerkRole());
        return roles;
    }
     
}