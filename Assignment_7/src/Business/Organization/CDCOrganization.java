/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.ClerkRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Tushar
 */
public class CDCOrganization extends Organization{

    public CDCOrganization() {
        super(Organization.Type.CDCOrganization.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new ClerkRole());
        return roles;
    }
     
}